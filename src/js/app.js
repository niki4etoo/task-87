import "../scss/app.scss";
import * as R from 'ramda';

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const arrayToPluck = [
    { name: "John", class: "is-primary" },
    { age: 23, class: "is-warning" },
    { job: "programmer", class: "is-danger" },
  ];
  var classes = R.pluck('class');
  classes = classes(arrayToPluck);
  console.log(classes);
  const articles = document.querySelectorAll("article");
  var i = 0;
  for(var article of articles){
		article.classList.add(classes[i++]);
		if(i == 3) i = 0;
  }
});
